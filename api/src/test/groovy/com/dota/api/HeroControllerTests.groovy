package com.dota.api

import com.dota.api.Errors.LimitExceeded
import com.dota.api.Errors.NotFoundHero
import com.dota.api.Errors.OffsetExceeded
import com.dota.api.Handler.RestExceptionHandler
import com.dota.api.Heroes.HeroController
import com.dota.api.Heroes.HeroService
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.util.LinkedMultiValueMap

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import spock.lang.Specification

class HeroControllerTests extends Specification {

    private HeroService heroService = Mock(HeroService)
    private HeroController heroController = new HeroController(heroService)
    private MockMvc mockMvc = MockMvcBuilders.standaloneSetup(heroController)
            .setControllerAdvice(new RestExceptionHandler())
            .build()

    def "Get Heroes should return a status code 200 when returns list of heroes"() {
        LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>()
        params.add("lane", "")
        params.add("difficult", "")
        params.add("offset", "0")
        params.add("limit", "10")

        when:
        ResultActions response = mockMvc.perform(get("/v1/heroes").params(params))

        then:
        response.andExpect(status().isOk())

    }

    def "Get Heroes should return a status code 404 when not found heroes"() {
        LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>()
        params.add("lane", "")
        params.add("difficult", "")
        params.add("offset", "0")
        params.add("limit", "10")

        given:
        this.heroService.getHeroes("", "", 0, 10) >> {
            throw new NotFoundHero("Nenhum heroi foi encontrado na base de dados")
        }

        when:
        ResultActions response = mockMvc.perform(get("/v1/heroes").params(params))

        then:
        response.andExpect(status().isNotFound())
    }

    def "Get heroes should return 400 when default limit is exceeded"() {
        LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>()
        params.add("lane", "")
        params.add("difficult", "")
        params.add("offset", "0")
        params.add("limit", "31")

        given:
        this.heroService.getHeroes("", "", 0, 31) >> {
            throw new LimitExceeded("Você esta colocando um limite acima do máximo permitido por padrao (default: 30)")
        }

        when:
        ResultActions response = mockMvc.perform(get("/v1/heroes").params(params))

        then:
        response.andExpect(status().isBadRequest())
        response.andExpect(content().json("{'errorMessage':'Você esta colocando um limite acima do máximo " +
                "permitido por padrao (default: 30)'}"))
    }

    def "Get heroes should return 200 when limit is valid"() {
        LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>()
        params.add("lane", "")
        params.add("difficult", "")
        params.add("offset", "0")
        params.add("limit", "10")

        when:
        ResultActions response = mockMvc.perform(get("/v1/heroes").params(params))

        then:
        response.andExpect(status().isOk())
    }

    def "Get heroes should return 400 when offset exceed heroes return quantity"() {
        LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>()
        params.add("lane", "")
        params.add("difficult", "")
        params.add("offset", "30")
        params.add("limit", "10")

        given:
        this.heroService.getHeroes("", "", 30, 10) >> {
            throw new OffsetExceeded("Offset colocado está acima da quantidade de herois retornados")
        }

        when:
        ResultActions response = mockMvc.perform(get("/v1/heroes").params(params))

        then:
        response.andExpect(status().isBadRequest())
        response.andExpect(content().json("{'errorMessage':'Offset colocado está acima da quantidade de " +
                "herois retornados'}"))
    }

    def "Get heroes should return 200 when offset is valid"() {
        LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>()
        params.add("lane", "")
        params.add("difficult", "")
        params.add("offset", "20")
        params.add("limit", "10")

        when:
        ResultActions response = mockMvc.perform(get("/v1/heroes").params(params))

        then:
        response.andExpect(status().isOk())
    }

    def "Create hero should return 200 and confirm when a hero is created"() {
        String request = "{}"
        given:

        when:
        ResultActions response = mockMvc.perform(post("/v1/heroes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))

        then:
        response.andExpect(status().isOk())
        response.andExpect(content().json("{'response':'O heroi foi criado com sucesso'}"))
    }

    def "Delete hero should return 200 when hero is deleted"() {
        when:
        ResultActions response = mockMvc.perform(delete("/v1/heroes/{id}", "51"))

        then:
        response.andExpect(status().isOk())
        response.andExpect(content().json("{'response':'Heroi deletado com sucesso'}"))
    }

    def "Delete hero should return 404 when not found a hero"() {
        given:
        this.heroService.deleteHero(51) >> {
            throw new NotFoundHero("Heroi nao foi encontrado pelo id informado")
        }

        when:
        ResultActions response = mockMvc.perform(delete("/v1/heroes/{id}", "51"))

        then:
        response.andExpect(status().isNotFound())
        response.andExpect(content().json("{'errorMessage':'Heroi nao foi encontrado pelo id informado'}"))
    }

    def "Edit hero should return 200 when hero is edited"() {
        String request = "{}"

        when:
        ResultActions response = mockMvc.perform(put("/v1/heroes/{id}", "51")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))

        then:
        response.andExpect(status().isOk())
        response.andExpect(content().json("{'response':'Editado com sucesso'}"))
    }
}
