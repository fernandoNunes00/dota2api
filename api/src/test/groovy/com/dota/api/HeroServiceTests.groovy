package com.dota.api

import com.dota.api.Errors.HeroInvalidField
import com.dota.api.Errors.LimitExceeded
import com.dota.api.Errors.NotFoundHero
import com.dota.api.Errors.OffsetExceeded
import com.dota.api.Handler.RestExceptionHandler
import com.dota.api.Heroes.Hero
import com.dota.api.Heroes.HeroService
import com.dota.api.Repository.HeroRepository
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

class HeroServiceTests extends Specification {

    private HeroRepository heroRepository = Mock(HeroRepository)
    private HeroService heroService = new HeroService(heroRepository)
    private MockMvc mockMvc = MockMvcBuilders.standaloneSetup(heroService)
            .setControllerAdvice(new RestExceptionHandler())
            .build()

    def "Get heroes should return a list of heroes"() {
        given:
        Hero hero1 = new Hero()
        List<Hero> heroes = new ArrayList<>()
        heroes.add(hero1)

        and:
        this.heroRepository.selectHeroes("%", "%", 0, 20) >> heroes

        when:
        List<Hero> result = this.heroService.getHeroes("", "", 0, 20)

        then:
        result != null
    }

    def "Get heroes should return not found when return hero is null or []"() {
        given:
        this.heroRepository.selectHeroes("%", "%", 0, 20) >> {
            throw new NotFoundHero("Heroi não encontrado")
        }

        when:
        List<Hero> result = this.heroService.getHeroes("", "", 0, 20)

        then:
        thrown(NotFoundHero)
    }

    def "Get heroes should return limit exceeded when limit is higher than 30"() {
        when:
        List<Hero> result = this.heroService.getHeroes("", "", 0, 31)

        then:
        thrown(LimitExceeded)
    }

    def "Get heroes should return offset exceeded when offset is higher than result"() {
        given:
        this.heroRepository.selectHeroes("%", "%", 2, 20) >> {
            throw new OffsetExceeded("Offset Exceeded")
        }

        when:
        List<Hero> result = this.heroService.getHeroes("", "", 2, 20)

        then:
        thrown(OffsetExceeded)
    }

    def "Create a hero should create a hero when fields are correctly"() {
        List<String> skills = new ArrayList<>()
        skills.add("Metamorfose")
        List<String> skins = new ArrayList<>()
        skins.add("Default")
        Hero hero = new Hero()
        hero.name = "Terrorblade"
        hero.lane = "safe"
        hero.difficult = "medium"
        hero.skills = skills
        hero.skins = skins

        when:
        this.heroService.createHero(hero)

        then:
        noExceptionThrown()
    }

    def "Create a hero should not create when field lane is null or invalid"() {
        List<String> skills = new ArrayList<>()
        skills.add("Metamorfose")
        List<String> skins = new ArrayList<>()
        skins.add("Default")
        Hero hero = new Hero()
        hero.name = "Terrorblade"
        hero.lane = "safeeeeee"
        hero.difficult = "medium"
        hero.skills = skills
        hero.skins = skins

        when:
        this.heroService.createHero(hero)

        then:
        thrown(HeroInvalidField)
    }

    def "Create a hero should not create when field difficult is null or invalid"() {
        List<String> skills = new ArrayList<>()
        skills.add("Metamorfose")
        List<String> skins = new ArrayList<>()
        skins.add("Default")
        Hero hero = new Hero()
        hero.name = "Terrorblade"
        hero.lane = "safe"
        hero.difficult = null
        hero.skills = skills
        hero.skins = skins

        when:
        this.heroService.createHero(hero)

        then:
        thrown(HeroInvalidField)
    }

    def "Create a hero should not create when field name is null"() {
        List<String> skills = new ArrayList<>()
        skills.add("Metamorfose")
        List<String> skins = new ArrayList<>()
        skins.add("Default")
        Hero hero = new Hero()
        hero.name = ""
        hero.lane = "safe"
        hero.difficult = "medium"
        hero.skills = skills
        hero.skins = skins

        when:
        this.heroService.createHero(hero)

        then:
        thrown(HeroInvalidField)
    }

    def "Create a hero should not create when field skills is null"() {
        List<String> skills = new ArrayList<>()
        skills.add("Metamorfose")
        List<String> skins = new ArrayList<>()
        skins.add("Default")
        Hero hero = new Hero()
        hero.name = "Terrorblade"
        hero.lane = "safe"
        hero.difficult = "medium"
        hero.skills = null
        hero.skins = skins

        when:
        this.heroService.createHero(hero)

        then:
        thrown(HeroInvalidField)
    }

    def "Create a hero should not create when field skins is null"() {
        List<String> skills = new ArrayList<>()
        skills.add("Metamorfose")
        List<String> skins = new ArrayList<>()
        skins.add("Default")
        Hero hero = new Hero()
        hero.name = "Terrorblade"
        hero.lane = "safe"
        hero.difficult = "medium"
        hero.skills = skills
        hero.skins = null

        when:
        this.heroService.createHero(hero)

        then:
        thrown(HeroInvalidField)
    }

    def "Edit hero should edit when id and fields are correctly"() {
        List<String> skills = new ArrayList<>()
        skills.add("Metamorfose")
        List<String> skins = new ArrayList<>()
        skins.add("Default")
        Hero hero = new Hero()
        hero.name = "Terrorblade"
        hero.lane = "safe"
        hero.difficult = "medium"
        hero.skills = skills
        hero.skins = skins

        when:
        this.heroService.editHero(1, hero)

        then:
        noExceptionThrown()
    }

    def "Edit hero should not edit when id is not found"() {

    }

    def "Edit hero should not edit when field lane is null or invalid"() {
        List<String> skills = new ArrayList<>()
        skills.add("Metamorfose")
        List<String> skins = new ArrayList<>()
        skins.add("Default")
        Hero hero = new Hero()
        hero.name = "Terrorblade"
        hero.lane = "safeeeeee"
        hero.difficult = "medium"
        hero.skills = skills
        hero.skins = skins

        when:
        this.heroService.editHero(1, hero)

        then:
        thrown(HeroInvalidField)
    }

    def "Edit hero should not edit when field difficult is null or invalid"() {
        List<String> skills = new ArrayList<>()
        skills.add("Metamorfose")
        List<String> skins = new ArrayList<>()
        skins.add("Default")
        Hero hero = new Hero()
        hero.name = "Terrorblade"
        hero.lane = "safe"
        hero.difficult = null
        hero.skills = skills
        hero.skins = skins

        when:
        this.heroService.editHero(1, hero)

        then:
        thrown(HeroInvalidField)
    }

    def "Edit hero should not edit when field name is null"() {
        List<String> skills = new ArrayList<>()
        skills.add("Metamorfose")
        List<String> skins = new ArrayList<>()
        skins.add("Default")
        Hero hero = new Hero()
        hero.name = ""
        hero.lane = "safe"
        hero.difficult = "medium"
        hero.skills = skills
        hero.skins = skins

        when:
        this.heroService.editHero(1, hero)

        then:
        thrown(HeroInvalidField)
    }

    def "Edit hero should not edit when field skills is null"() {
        List<String> skills = new ArrayList<>()
        skills.add("Metamorfose")
        List<String> skins = new ArrayList<>()
        skins.add("Default")
        Hero hero = new Hero()
        hero.name = "Terrorblade"
        hero.lane = "safe"
        hero.difficult = "medium"
        hero.skills = null
        hero.skins = skins

        when:
        this.heroService.editHero(1, hero)

        then:
        thrown(HeroInvalidField)
    }

    def "Edit hero should not edit when field skins is null"() {
        List<String> skills = new ArrayList<>()
        skills.add("Metamorfose")
        List<String> skins = new ArrayList<>()
        skins.add("Default")
        Hero hero = new Hero()
        hero.name = "Terrorblade"
        hero.lane = "safe"
        hero.difficult = "medium"
        hero.skills = skills
        hero.skins = null

        when:
        this.heroService.editHero(1, hero)

        then:
        thrown(HeroInvalidField)
    }

    def "Delete hero when id is found"() {
        when:
        this.heroService.deleteHero(1)

        then:
        noExceptionThrown()
    }

    def "Delete hero not delete when id is not found"() {
        given:
        this.heroRepository.delete(1) >> {
            throw new NotFoundHero("Não foi encontrado o ID")
        }

        when:
        this.heroService.deleteHero(1)

        then:
        thrown(NotFoundHero)
    }
}
